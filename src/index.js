import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Footer from './components/pages/static/Footer'
import reportWebVitals from './reportWebVitals';
ReactDOM.render(
  <React.StrictMode>
    <App />
    <Footer />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
