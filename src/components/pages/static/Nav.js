import React, { useState, useEffect } from 'react';
import venya from '../../Images/venyalogo.png';
import MenuIcon from '@mui/icons-material/Menu';
import CloseIcon from '@mui/icons-material/Close';
import Navdrawer from '../../Modals/Navdrawer';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { NavLink } from 'react-router-dom';
import { useWindowScroll } from 'react-use';
import Login from '../Authetication/Login';


const Nav = () => {
    const { y: pageYOffset } = useWindowScroll();
    const [drawer, setDrawer] = useState(false);
    const [navdesktop, setNavDesktop] = useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [openmodel, setOpenModel] = React.useState(false);
    const handleModelOpen = () => setOpenModel(true);
    const handleModelClose = () => setOpenModel(false);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };


    useEffect(() => {
        if (pageYOffset >= 10) {
            setNavDesktop(true);
        }
        else {
            setNavDesktop(false);
        }
    }, [pageYOffset])
    return (
        <>
            <nav className='navmobile'>
                <div>
                    <img src={venya} alt='venya' />
                </div>
                <div>
                    <button
                        onClick={() => setDrawer(!drawer)}
                    >
                        {
                            drawer ? <CloseIcon /> : <MenuIcon />
                        }
                    </button>
                </div>
            </nav >

            <nav className={navdesktop ? "navdesktop navdesktop-active" : "navdesktop navdesktop-notactive"} >
                <Button
                    sx={{ color: "white" }}
                    id="basic-button"
                    aria-controls={open ? 'basic-menu' : undefined}
                    aria-haspopup="true"
                    aria-expanded={open ? 'true' : undefined}
                    onClick={handleClick}
                >
                    About us
                </Button>
                <Menu
                    id="basic-menu"
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}
                    MenuListProps={{
                        'aria-labelledby': 'basic-button',
                    }}
                >
                    <NavLink to="/ourstory">
                        <MenuItem onClick={handleClose}>
                            Our story
                        </MenuItem>
                    </NavLink>
                    <NavLink to="/corevalues">
                        <MenuItem onClick={handleClose}>
                            Core values
                        </MenuItem>
                    </NavLink>
                    <NavLink to="/meettheteam">
                        <MenuItem onClick={handleClose}>
                            Meet the team
                        </MenuItem>
                    </NavLink>
                    <NavLink to="/press">
                        <MenuItem onClick={handleClose}>
                            Press
                        </MenuItem>
                    </NavLink>
                    <NavLink to="/investor">
                        <MenuItem onClick={handleClose}>
                            Investor
                        </MenuItem>
                    </NavLink>

                </Menu>
                <NavLink to="/rentahome">Rent a home</NavLink>
                <a href='/' >
                    <img src={venya} alt='venya' />
                </a>
                <a href='/'  >Contact</a>
                <Button
                    sx={{ color: "white" }}
                    onClick={handleModelOpen}>Login</Button>
            </nav>
            <Navdrawer state={drawer} toggleDrawer={() => setDrawer(false)} />
            <Login
                handleOpen={openmodel}
                handleClose={handleModelClose}
            />
        </>
    )
};

export default Nav;
