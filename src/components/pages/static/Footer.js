import React from 'react';
import { Facebook, Twitter, Linkedin, Youtube } from 'react-bootstrap-icons';
import venya from '../../Images/venyalogo.png';
const Footer = () => {
    return (
        <>
            <section className="share-venya">
                <h5>#VisitTheVenya</h5>
                <p>Share Your Photos abd
                    adventures for a chance to get <br />featured by using the #visit The Venya #</p>
                <h5>FOLLOW US:</h5>
                <div className='social-media'>
                    <a href='#'>
                        <Twitter />
                    </a>
                    <a href='#'>
                        <Facebook />
                    </a>
                    <a href='#'>
                        <Linkedin />
                    </a>
                    <a href='#'>
                        <Youtube />
                    </a>
                </div>

            </section>
            <footer>
                <section className='footer'>
                    <div>
                        <img src={venya} alt="venya" />
                        <h4>Inspiration in Your Inbox</h4>
                        <p>Be the first to hear about exclusive events, immersive
                            <br />experiences,
                            new villas, and more.
                        </p>
                        <form action="#" method="post" className='input-form-footer'>

                            <input type="email" id="email" placeholder="Enter email" />
                            <button type="submit" >Submit</button>

                        </form>
                    </div>
                    <div className='contenttwo'>
                        <div>
                            <h6>Support</h6>
                            <ul className='list-footer'>
                                <li>
                                    <a href="#">Terms &amp; Conditions</a>

                                </li>
                                <li>
                                    <a href="#">Privacy &amp; Policy</a>

                                </li>
                                <li>

                                    <a href="#">FAQ</a>
                                </li>
                                <li>

                                    <a href="#">Login</a>
                                </li>
                            </ul>
                        </div>
                        <div>
                            <h6>Discover</h6>
                            <ul className='list-footer'>

                                <li>

                                    <a href="#">Home</a>
                                </li>
                                <li>

                                    <a href="#">About Us</a>
                                </li>
                                <li>

                                    <a href="#">Destinations</a>
                                </li>
                                <li>

                                    <a href="#">Rent a Home</a>
                                </li>
                                <li>

                                    <a href="#">Own a Home</a>
                                </li>
                                <li>

                                    <a href="#">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div>
                        <h6>Connect Us</h6>

                        <ul className='list-footer'>
                            <li>

                                <a href="tel:+91-0000000000">Contact:1578546584</a><br />
                            </li>
                            <li>

                                <a href="mailto:support@gmail.com">Email:support@gmail.com</a>
                            </li>
                        </ul>

                        <div className='social-media'>
                            <a href='#'>
                                <Twitter />
                            </a>
                            <a href='#'>
                                <Facebook />
                            </a>
                            <a href='#'>
                                <Linkedin />
                            </a>
                            <a href='#'>
                                <Youtube />
                            </a>
                        </div>
                    </div>
                </section>

            </footer>
        </>
    );
};

export default Footer;
