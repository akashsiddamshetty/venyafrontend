import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
const Login = ({ handleOpen, handleClose }) => {
    return (
        <div>
            <Modal
                open={handleOpen}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <section className='login'>
                        <h4>LOGIN TO THE VENYA</h4>
                        <form className='login-form'>
                            <div className="form-group">
                                <input type="email" className="input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email/mobile" />
                            </div>
                            <div className="form-group">
                                <input type="password" className="input" id="exampleInputPassword1" placeholder="Enter your Password" />
                            </div>
                            <div>
                                <button type="submit" className="">LOGIN</button>

                            </div>
                            <span>OR</span>
                            <div>
                                <button type="submit" className="">GET OTP</button>
                            </div>
                            <p>New user? <span>Sign up</span></p>
                        </form>
                    </section>
                </Box>
            </Modal>
        </div>
    )
}

export default Login