import React, { useState, useEffect } from 'react';
import Skeleton from '@mui/material/Skeleton';
const SectionC = () => {
    const [sectioncdata, setSectioncData] = useState([]);
    const [loading, setLoading] = useState(false);
    const fecthData = async () => {
        const res = await fetch(`${process.env.REACT_APP_BASE_URL}/portal/down-image/index/`)
        const sectioncdata = await res.json()
        setSectioncData(sectioncdata.data)
        setLoading(true);
    }
    useEffect(() => {
        fecthData()
    }, [])
    return (
        <>
            <section className='home-sectionC'>
                <h3>About The Venya</h3>

            </section>
            {loading ? (
                <section className='aboutuscontainer' >
                    {sectioncdata.map((sectioncdatainfo, i) => {
                        return (
                            <div key={i} className={`content ${(i % 2 === 0) ? 'reverse' : undefined}`}>
                                <div>
                                    <img height="350px" src={`${process.env.REACT_APP_BASE_URL}${sectioncdatainfo.image}`} alt="" />
                                </div>
                                <div>
                                    <h3><span className="trending-head">{sectioncdatainfo.title}</span>
                                    </h3>
                                    <p>{sectioncdatainfo.description}</p>
                                </div>

                            </div>
                        )
                    })

                    }

                </section >
            ) :
                (

                    <Skeleton />
                )}
        </>



    );
};

export default SectionC;
