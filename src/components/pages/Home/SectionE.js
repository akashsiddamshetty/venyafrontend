import { Skeleton } from '@mui/material';
import React, { useState, useEffect } from 'react'

const SectionE = () => {
    const [homes, setHome] = useState([]);
    const [state, setState] = useState("0");
    const [loading, setLoading] = useState(false);
    const fecthData = async () => {
        const res = await fetch(`${process.env.REACT_APP_BASE_URL}/portal/home-suited/`)
        const homes = await res.json()
        setHome(homes.data)
        setLoading(true);
    }
    useEffect(() => {
        fecthData()
    }, [])
    return (
        <section className='sectionE'>
            <h1>
                Homes suited for you
            </h1>
            {
                loading ?
                    (
                        <>
                            <section className='mobiletab'>

                                <div
                                    className='mobiletab-infosection'
                                    style={{ backgroundImage: `url(${process.env.REACT_APP_BASE_URL}${homes[state].image})` }}
                                >
                                    <h4>{homes[state].title}</h4>
                                    <p>{homes[state].description}</p>
                                    <a href={homes[state].link} rel="noreferrer" target="_blank" ><button>Know more</button></a>
                                </div>
                                <div className='mobiletab-buttoncontainer'>
                                    {homes.map((button, i) => {
                                        return (
                                            <button
                                                className='owl-dot'
                                                key={i}
                                                id={button.count}
                                                onClick={() => setState(i)}
                                            ></button>
                                        )
                                    })}
                                </div>
                            </section>
                            <section className='laptopsuited'>

                                {homes.map((homesinfo, j) => {
                                    return (
                                        <div
                                            className='laptopsuited-content'
                                            style={{ backgroundImage: `url(${process.env.REACT_APP_BASE_URL}${homesinfo.image})` }}
                                        >
                                            <h4>{homesinfo.title}</h4>
                                            <p>{homesinfo.description}</p>
                                            <a>Know more</a>
                                        </div>
                                    )
                                })}
                            </section>
                        </>
                    )
                    :
                    (
                        <Skeleton />
                    )
            }

        </section>
    )
}

export default SectionE