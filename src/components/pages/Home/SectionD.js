import React, { useState, useEffect } from 'react';
import Skeleton from '@mui/material/Skeleton';

const SectionD = () => {
    const [featured, setFeatured] = useState([]);
    const [loading, setLoading] = useState(false);
    const [state, setState] = useState("0");
    const fecthData = async () => {
        const res = await fetch(`${process.env.REACT_APP_BASE_URL}/portal/featured-villas/`)
        const featured = await res.json()
        setFeatured(featured.data)
        setLoading(true);
    }
    useEffect(() => {
        fecthData()
    }, [])
    console.log(featured)
    return (
        <div className='SectionD'>
            {
                loading ? (
                    <>
                        <div className='SectionD-container'>
                            <div>
                                <h3>
                                    <span className='venay-head'>Featured Villas</span>
                                </h3>
                                <img src={`${process.env.REACT_APP_BASE_URL}${featured[state].image}`} alt='featured' />
                            </div>
                            <div className='SectionD-container-text'>

                                <h3><span className='venay-head'>{featured[state].title}</span></h3>
                                <p>{featured[state].description}</p>
                                <button>Discover</button>
                            </div>
                        </div>
                        <div className='SectionD-buttoncontainer'>
                            {featured.map((button, i) => {
                                return (
                                    <button
                                        className='owl-dot'
                                        key={i}
                                        id={button.count}
                                        onClick={() => setState(i)}
                                    ></button>
                                )
                            })}
                        </div>
                    </>

                )
                    : (
                        <Skeleton />
                    )
            }

        </div>
    )
}

export default SectionD
