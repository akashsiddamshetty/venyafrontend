import React from 'react';
import SectionA from './SectionA';
import SectionB from './SectionB';
import SectionC from './SectionC';
import SectionD from './SectionD';
import SectionE from './SectionE';
const Home = () => {


    return (
        <>
            <div className='home'>
                <SectionA />
                <SectionB />
                <SectionC />
                <SectionD />
                <SectionE />
            </div>
        </>
    );
}

export default Home;
