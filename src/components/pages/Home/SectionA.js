import React, { useState, useEffect } from 'react';
import mountain from '../../Images/mountain.jpg';
import Paragliding from '../../Images/paragliding.jpg';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import lightv2 from '../../Images/overlay-section-light_v2.png';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
const SectionA = () => {
    const [state, setState] = React.useState(0);
    const [images, setImages] = useState([]);
    const [loading, setLoading] = useState(false);
    const fecthData = async () => {
        const res = await fetch(`${process.env.REACT_APP_BASE_URL}/portal/front-video-or-image/index/`)
        const images = await res.json()
        setImages(images.data)
        setLoading(true);
    }
    useEffect(() => {
        fecthData()
    }, [])

    const length = images.length;
    const next = () => {
        setState(state === length - 1 ? 0 : state + 1)
    };

    const prev = () => {
        setState(state === 0 ? length - 1 : state - 1)
    }
    return (
        <>

            {loading ? (
                <div className='home-sectionA' style={{ backgroundImage: `url(${process.env.REACT_APP_BASE_URL}${images[state].top_image_or_video})` }} >
                    <div className='home-sectionA-info'>
                        <button
                            onClick={prev}
                        >
                            <ArrowBackIcon />
                        </button>
                        <div className='home-sectionA-info-text'>
                            <h1>New Places. New Spaces</h1>
                            <button>RENT A HOME</button>
                        </div>
                        <button
                            onClick={next}
                        >
                            <ArrowForwardIcon />
                        </button>
                        {/* <img src={lightv2} alt='light v2 overlay' /> */}
                    </div>
                </div>
            ) :
                (
                    <div className='home-sectionA-loading'>
                        <Box sx={{ display: 'flex', justifyContent: 'center', top: 200 }}>
                            <CircularProgress style={{ marginTop: '25%' }} />
                        </Box>
                    </div>

                )
            }
        </>
    );

};
export default SectionA;
