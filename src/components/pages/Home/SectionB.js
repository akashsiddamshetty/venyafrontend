import React from 'react';
import venya from '../../Images/venyalogo.png';
const SectionB = () => {
    return (
        <>
            <div className='home-sectionB'>

                <div className='home-sectionB-info'>
                    <h5> Lovable, when you wish for something, learn for something from above </h5>
                    <img src={venya} alt='venyalogo' />
                    <h1>A Journey that goes </h1>
                    <span>Beyond its destinations </span>
                    <p>The Venya caters to a need and desire in all of us
                        to be truly transformed. It is a symphony of experiences,
                        architecture and cultures that harmoniously blend with social
                        responsibility and technologically driven living. </p>
                </div>
                <div className='home-sectionB-cards'>
                    <div className='home-sectionB-cards-card'>
                        <h3>Holistic Hospitality</h3>
                        <p>Our intent is to relook at wellness luxur
                            form a 6th sense that digs deeper into your
                            mind body and soul.
                        </p>
                        <div>yoga</div>
                        <div>Spa Treatments</div>
                        <div>Organic Cuisine</div>
                    </div>
                    <div className='home-sectionB-cards-card'>
                        <h3>Holistic Hospitality</h3>
                        <p>Our intent is to relook at wellness luxur
                            form a 6th sense that digs deeper into your
                            mind body and soul.
                        </p>
                        <div>yoga</div>
                        <div>Spa Treatments</div>
                        <div>Organic Cuisine</div>
                    </div>
                    <div className='home-sectionB-cards-card'>
                        <h3>Holistic Hospitality</h3>
                        <p>Our intent is to relook at wellness luxur
                            form a 6th sense that digs deeper into your
                            mind body and soul.
                        </p>
                        <div>yoga</div>
                        <div>Spa Treatments</div>
                        <div>Organic Cuisine</div>
                    </div>

                </div>

            </div>
        </>
    );
};

export default SectionB;
