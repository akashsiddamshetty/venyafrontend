import React, { useState, useEffect } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Downimages from './Downimages';
import Box from '@mui/material/Box';
const Corevalues = () => {
  const [corevaluesimages, setCoreValuesImages] = useState([]);
  const [loading, setLoading] = useState(false);
  const fecthData = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/portal/front-video-or-image/index/`)
    const corevaluesimages = await res.json()
    setCoreValuesImages(corevaluesimages.data)
    setLoading(true);
    console.log(corevaluesimages);
  }

  useEffect(() => {
    fecthData()
  }, [])
  return (
    <>
      {loading ? (
        <div className='topsection' style={{ backgroundImage: `url(${process.env.REACT_APP_BASE_URL}${corevaluesimages[0].top_image_or_video})` }} >

          <h1>
            Our Core Values
          </h1>

        </div>
      )
        :
        (
          <div className='home-sectionA-loading'>
            <Box sx={{ display: 'flex', justifyContent: 'center', top: 200 }}>
              <CircularProgress style={{ marginTop: '25%' }} />
            </Box>
          </div>
        )
      }

      <Downimages />
      {/* <img src={lightv2} alt='light v2 overlay' /> */}
    </>
  )
}

export default Corevalues


