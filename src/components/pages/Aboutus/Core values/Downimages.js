import React, { useState, useEffect } from 'react';
import Skeleton from '@mui/material/Skeleton';
const Downimages = () => {
    const [downimages, setDownImages] = useState([]);
    const [loading, setLoading] = useState(false);
    const fecthData = async () => {
        const res = await fetch(`${process.env.REACT_APP_BASE_URL}/portal/down-image/our story/`)
        const downimages = await res.json()
        setDownImages(downimages.data)
        setLoading(true);
        console.log(downimages);
    }
    useEffect(() => {
        fecthData()
    }, [])
    return (
        <>
            {loading ? (
                <section className='aboutuscontainer' >
                    {downimages.map((downimagesinfo, i) => {
                        return (
                            <div key={i} className={`content ${(i % 2 === 0) ? 'reverse' : undefined}`}>
                                <div>
                                    <img height="350px" src={`${process.env.REACT_APP_BASE_URL}${downimagesinfo.image}`} alt="" />
                                </div>
                                <div>
                                    <h3><span className="trending-head">{downimagesinfo.title}</span>
                                    </h3>
                                    <p>{downimagesinfo.description}</p>
                                </div>

                            </div>
                        )
                    })

                    }

                </section >
            ) :
                (

                    <Skeleton />
                )}
        </>
    )
}
export default Downimages

