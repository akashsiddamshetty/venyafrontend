import React, { useState, useEffect } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import Team from './Team'
const Meettheteam = () => {
  const [images, setImages] = useState([]);
  const [loading, setLoading] = useState(false);
  const fecthData = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}/portal/front-video-or-image/our story/`)
    const images = await res.json()
    setImages(images.data)
    setLoading(true);
  }
  useEffect(() => {
    fecthData()
  }, [])
  return (
    <>
      {loading ? (
        <div className='topsection' style={{ backgroundImage: `url(${process.env.REACT_APP_BASE_URL}${images[0].top_image_or_video})` }} >

          <h1>
            Our Team
          </h1>

          {/* <img src={lightv2} alt='light v2 overlay' /> */}
        </div>
      )
        :
        (
          <div className='home-sectionA-loading'>
            <Box sx={{ display: 'flex', justifyContent: 'center', top: 200 }}>
              <CircularProgress style={{ marginTop: '25%' }} />
            </Box>
          </div>
        )
      }
      <Team />
    </>
  )
}

export default Meettheteam