import React, { useState, useEffect } from 'react'
import Skeleton from '@mui/material/Skeleton';

const Pressarticles = () => {
    const [press, setPress] = useState([]);
    const [loading, setLoading] = useState(false);
    const fecthData = async () => {
        const res = await fetch(`${process.env.REACT_APP_BASE_URL}/portal/press/`)
        const press = await res.json()
        setPress(press.data);
        setLoading(true);
    }
    useEffect(() => {
        fecthData()
    }, [])
    return (
        <>
            {
                loading ?
                    (
                        <section className='press'>
                            {press.map((Pressarticles, i) => {
                                return (
                                    <div className='pressarticles' key={i}>
                                        {
                                            <img src={`${process.env.REACT_APP_BASE_URL}${Pressarticles.image}`} alt='' />

                                        }

                                        <div className='pressinfo'>
                                            <h1>{Pressarticles.title}</h1>
                                            <p>{Pressarticles.text}</p>
                                            <a href={Pressarticles.link} rel="noreferrer" target="_blank"><button>link</button></a>
                                        </div>
                                    </div>

                                )
                            })}
                        </section>

                    ) :
                    (
                        <Skeleton variant="rectangular" width="100%" height={400} />
                    )
            }
        </>
    )
}

export default Pressarticles