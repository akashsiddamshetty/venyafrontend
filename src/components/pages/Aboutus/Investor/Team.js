import React, { useState, useEffect } from 'react';
import Skeleton from '@mui/material/Skeleton';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const Team = () => {
    const [team, setTeam] = useState([]);
    const [loading, setLoading] = useState(false);
    const [state, setState] = useState("0");
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const fecthData = async () => {
        const res = await fetch(`${process.env.REACT_APP_BASE_URL}/portal/team/`)
        const team = await res.json()
        setTeam(team.data);
        setLoading(true);
    }
    useEffect(() => {
        fecthData()
    }, [])
    console.log(state)
    return (
        <>
            {
                loading ?
                    (
                        <section className='team'>
                            {team.map((teaminfo, i) => {
                                return (
                                    <div className='team-members' key={i}>
                                        {
                                            loading ? (
                                                <img src={`${process.env.REACT_APP_BASE_URL}${teaminfo.picture}`} alt='' />
                                            ) :
                                                <Skeleton />
                                        }

                                        <h1>{teaminfo.title} {teaminfo.full_name}</h1>
                                        <h3>{teaminfo.designation}</h3>
                                        <button
                                            onClick={() => { setState(i); handleOpen() }}
                                        >Know more</button>
                                    </div>

                                )
                            })}
                        </section>

                    ) :
                    (
                        <Skeleton variant="rectangular" width="100%" height={400} />
                    )
            }
            {
                loading ? (
                    <div>
                        <Modal
                            open={open}
                            onClose={handleClose}
                            aria-labelledby="modal-modal-title"
                            aria-describedby="modal-modal-description"
                        >
                            <Box sx={style}>
                                <div className='team-members'>
                                    <img src={`${process.env.REACT_APP_BASE_URL}${team[state].picture}`} alt='' />
                                    <h1>{team[state].title} {team[state].full_name}</h1>
                                    <h3>{team[state].designation}</h3>

                                    <p>
                                        {team[state].description}
                                    </p>
                                </div>
                            </Box>
                        </Modal>
                    </div>
                ) :

                    <Skeleton />

            }


        </>
    )
}

export default Team