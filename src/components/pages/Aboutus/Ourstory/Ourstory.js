import React, { useState, useEffect } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import Downimages from './Downimages';
const Ourstory = () => {
    const [images, setImages] = useState([]);
    const [loading, setLoading] = useState(false);
    const fecthData = async () => {
        const res = await fetch(`${process.env.REACT_APP_BASE_URL}/portal/front-video-or-image/our story/`)
        const images = await res.json()
        setImages(images.data)
        setLoading(true);
    }
    useEffect(() => {
        fecthData()
    }, [])
    return (
        <>
            {loading ? (
                <div className='topsection' style={{ backgroundImage: `url(${process.env.REACT_APP_BASE_URL}${images[0].top_image_or_video})` }} >

                    <h1>Our Story</h1>

                    {/* <img src={lightv2} alt='light v2 overlay' /> */}
                </div>
            )
                :
                (
                    <div className='home-sectionA-loading'>
                        <Box sx={{ display: 'flex', justifyContent: 'center', top: 200 }}>
                            <CircularProgress style={{ marginTop: '25%' }} />
                        </Box>
                    </div>
                )
            }
            <div className="mission-section">
                <h6>OUR MISSION</h6>
                <h3>Why we're here</h3>
                <p className="v39 mt-4">The Venya caters to a need and desire in all of us to be truly transformed. It is a symphony of experiences, architecture and cultures that harmoniously blend with social responsibility and technologically driven living. </p>
            </div>

            <Downimages />
        </>
    )
}

export default Ourstory