import * as React from 'react';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import { makeStyles } from '@mui/styles';
import { List, ListItem, } from '@mui/material'
import { NavLink } from 'react-router-dom'
const useStyles = makeStyles(theme => ({
    root: {
        "& .MuiPaper-root": {
            top: "80px",
            background: "black",
            color: "white"
        },
        "& .MuiBackdrop": {
            margintop: "80px"
        }
    },
}));

export default function Navdrawer({ state, toggleDrawer }) {

    const classes = useStyles();

    return (

        <SwipeableDrawer
            anchor='top'
            open={state}
            onClose={toggleDrawer}
            onOpen={toggleDrawer}
            className={classes.root} >
            <div className='navmobile-drawer'>
                <List>
                    Aboutus
                </List>
                <NavLink to='/ourstory'>
                    <ListItem>
                        Our story
                    </ListItem>
                </NavLink>
                <NavLink to='/corevalues'>
                    <ListItem>
                        Core values
                    </ListItem>
                </NavLink>
                <NavLink to='/meettheteam'>
                    <ListItem>
                        Meet the tean
                    </ListItem>
                </NavLink>
                <NavLink to='/press'>
                    <ListItem>
                        Press
                    </ListItem>
                </NavLink>
                <NavLink to='/investors'>
                    <ListItem>
                        Investors
                    </ListItem>
                </NavLink>
                <NavLink to="/rentahome">Rent a home</NavLink>
            </div>
        </SwipeableDrawer >

    );
}