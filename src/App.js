import './components/scss/style.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Nav from './components/pages/static/Nav';
import Home from './components/pages/Home/Home';
import Ourstory from './components/pages/Aboutus/Ourstory/Ourstory';
import Corevalues from './components/pages/Aboutus/Core values/Corevalues';
import Meettheteam from './components/pages/Aboutus/Meet the team/Meettheteam';
import Press from './components/pages/Aboutus/Press/Press';
import Investor from './components/pages/Aboutus/Investor/Investor';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Rentahome from './components/pages/Rentahome/Rentahome';
function App() {
  return (
    <>
      <Router>
        <Nav />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/rentahome" element={<Rentahome />} />
          <Route exact path="/ourstory" element={<Ourstory />} />
          <Route exact path="/corevalues" element={<Corevalues />} />
          <Route exact path="/meettheteam" element={<Meettheteam />} />
          <Route exact path="/press" element={<Press />} />
          <Route exact path="/investor" element={<Investor />} />
        </Routes>
      </Router>
    </>
  )
};

export default App;
